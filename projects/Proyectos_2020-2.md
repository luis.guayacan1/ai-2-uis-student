---
# Proyectos 2020-2. Inteligencia Artificial II: Deep Learning

## Prof: Fabio Martínez, Ph.D

---

# Lista de Proyectos
1. [Detección de Retinopatía Diabética y sus etapas](#proy1)
2. [CNN-CANCER_CELLS_DECTECTION](#proy2)
3. [TrashContainersAI](#proy3)
4. [Clasificación de especies de aves usando redes neuronales convolucionales](#proy4)
5. [Detección de comportamientos distractores al conducir un automóvil.](#proy5)
6. [Tweets Sentiment Analysis and Tweet Generator using NLP](#proy6)
7. [CNN al volante](#proy7)
8. [Face mask detection using YOLOv5](#proy8)
9. [Clasificación de estrellas](#proy9)
10. [Bees or Wasp” Dataset Classification using CNN’s with Transfer Learning via Fine-tuning](#proy10)
11. [FACE MASK DETECTION](#proy11)
---

## Detección de Retinopatía Diabética y sus etapas <a name="proy1"></a>

**Autores: Jenny Marcela Santamaría Rincón, Nelson Alexis Cáceres Carreño	**

<img src="https://raw.githubusercontent.com/Alexis3004/Deteccion_Retinopatia_Diabetica/main/BannerRetinopatia.jpeg" style="width:700px;">

**Objetivo: Clasificar las imágenes de fondo de ojo en sanas o con retinopatía diabética sin especificar el grado de la enfermedad si la padece.**




- Dataset: El conjunto de datos consta de 35.126 imágenes de escaneo de retina tomadas en un exámen de fondo de ojo, tanto del ojo izquierdo como del derecho. Dichas imágenes se categorizan dependiendo el estado o etapa en la cual se encuentra esta enfermedad ocular:. 
- Modelo: VGG16, Xception, InceptionResnetV2, MobileNetV2


[(code)](https://github.com/Alexis3004/Deteccion_Retinopatia_Diabetica) [(video)](https://www.youtube.com/watch?v=5XMXb5rqnMM) [(+info)](https://github.com/Alexis3004/Deteccion_Retinopatia_Diabetica/blob/main/Deteccion_RD.pdf)


---

## CNN-CANCER_CELLS_DECTECTION <a name="proy2"></a>

**Autores: Jorge Alfredo Jaimes Teherán, Alejandro José Espinel Perez**

<img src="https://github.com/Jorge-Jaimes/CNN-CANCER_CELLS_DECTECTION/blob/main/banner.png" style="width:700px;">

**Objetivo: Detección de células cancerigenas**




- Dataset: This project use a dataset from a competetion of kaggle, you will find in the following [link](https://www.kaggle.com/c/histopathologic-cancer-detection/data). The images used were 10.000 images choosen randomly
- Modelo: CNN-models


[(code)](https://github.com/Jorge-Jaimes/CNN-CANCER_CELLS_DECTECTION) [(video)](https://drive.google.com/file/d/1-r0aEmA8hgREnjPI6cZyTZyrDFuXEYr3/view) [(+info)](https://github.com/Jorge-Jaimes/CNN-CANCER_CELLS_DECTECTION)



---

## TrashContainersAI <a name="proy3"></a>

**Autores: Juan Ricardo Albarracin, Christian Rengifo**

<img src="https://raw.githubusercontent.com/juanrtato/TrashContainersAI/main/Banner.jpg" style="width:700px;">

**Objetivo: Clasificador de imágenes de contenedores de basura en mal estado usando técnicas y metodologías de Inteligencia Artificial.**




- Dataset: Conjunto de datos de [Montevideo](https://www.kaggle.com/rodrigolaguna/clean-dirty-containers-in-montevideo)
- Modelo: MObilenet


[(code)](https://github.com/juanrtato/TrashContainersAI) [(video)](https://drive.google.com/file/d/1EB8sIRKNKg4qIVvgPy0lLhlO1kka6nVZ/view) [(+info)](https://github.com/juanrtato/TrashContainersAI)


---

## Clasificación de especies de aves usando redes neuronales convolucionales <a name="proy4"></a>

**Autores: Santiago Angulo Flórez, Jean Carlos Portilla Mora	**

<img src="https://raw.githubusercontent.com/Parhy/Birds/master/BannerIA2.png" style="width:700px;">

**Objetivo: Realizar una clasificación de diferentes especies de aves a través de una serie de imágenes, e identificar especies que puedan ser confundidas con otras a causa de su parecido físico.**




- Dataset: Bird Species. 
- Modelo: MobileNet and ResNet.


[(code)](https://github.com/Parhy/Birds) [(video)](https://www.youtube.com/watch?v=F9ZSR4qR6nQ&feature=youtu.be) [(+info)](https://github.com/Parhy/Birds/blob/main/Clasificaci%C3%B3n%20de%20aves%20.pptx)

---

## Detección de comportamientos distractores al conducir un automóvil. <a name="proy5"></a>

**Autores: Kevin Javier Lozano Galvis, Brayan Rofoldo Barajas Ochoa**

<img src="https://github.com/KevinLozanoG/Proyecto-Inteligencia-Artificial-II/raw/main/Banner/Banner.png" style="width:700px;">

**Objetivo: Aplicar los métodos de Deep Learning para detectar distracciones manuales a partir de la actividad que realizan los conductores.**


- Dataset: [State Farm Distracted Driver Detection](https://www.kaggle.com/c/state-farm-distracted-driver-detection)
- Modelo: CNN, VGG16, VGG19, DenseNet169.



[(code)](https://github.com/KevinLozanoG/Proyecto-Inteligencia-Artificial-II) [(video)](https://youtu.be/W05nYIIqyi4) [(+info)](https://github.com/KevinLozanoG/Proyecto-Inteligencia-Artificial-II/tree/main/Slides)

---

## Tweets Sentiment Analysis and Tweet Generator using NLP <a name="proy6"></a>

**Autores: Fredy Alejandro Mendoze, Christian Ruiz Lagos**

<img src="https://github.com/chrismcruiz/Tweets-Sentiment-Analysis-and-Tweet-Generator/raw/main/banner.jpeg?raw=true" style="width:700px;">

**Objetivo: Realizar un proceso de Web Scrapping para la recolección de datos en Twitter.**




- Dataset: El dataset se obtuvo a partir de Web Scrapping. Para ello, recopilamos tweets relacionados con la vacuna del COVID-19. Para el clasificador, con el fin de realizar un entrenamiento supervisado, recolectamos tweets a partir de los hashtags #YoMeVacuno y #YoNoMeVacuno. Luego de obtener los tweets, realizamos un análisis y aplicamos medidas para obtener solo la información deseada.
- Modelo: LSTM, GRU, GPT-2-small-spanish


[(code)](https://github.com/chrismcruiz/Tweets-Sentiment-Analysis-and-Tweet-Generator) [(video)](https://www.youtube.com/watch?v=L4V6qdpRNL0) [(+info)](https://huggingface.co/datificate/gpt2-small-spanish)

---

## CNN al volante <a name="proy7"></a>

**Autores:Juan Arango, Cristian Camacho, Camilo Sanmiguel**

<img src="https://github.com/JuanArango99/CNN-al-volante/blob/master/Banner.jpg?raw=true" style="width:700px;">

**Objetivo: simular un entorno de manejo de vehiculos dentro del videojuego Grand Theft Auto V en el que se pretende darle la maniobrabilidad del vehículo a la propia máquina con la ayuda de Redes Neuronales Convolocionales.**




- Dataset: Propio
- Modelo: CNN


[(code)](https://github.com/JuanArango99/CNN-al-volante) [(video)](https://www.youtube.com/watch?v=r6djn1QbB7I&feature=youtu.be) [(+info)](https://github.com/JuanArango99/CNN-al-volante)

---

## Face mask detection using YOLOv5 <a name="proy8"></a>

**Autores: David Morales, santiago castro**

<img src="https://github.com/David-Morales-Norato/face-mask-detection-yolov5/raw/main/images/banner.png" style="width:700px;">

**Objetivo: Detección de mascarillas en tiempo real**




- Dataset: [ace-mask-detection-dataset](https://www.kaggle.com/wobotintelligence/face-mask-detection-dataset)
- Modelo: Yolo5


[(code)](https://github.com/David-Morales-Norato/face-mask-detection-yolov5) [(video)](https://www.youtube.com/watch?v=cezkQbWax6Q) [(+info)](https://github.com/David-Morales-Norato/face-mask-detection-yolov5)

---

## Variable Cepheid classification from unprocessed light  <a name="proy9"></a>

**Autores: Maximiliano Garavito**

<img src="https://github.com/HiperMaximus/Proyecto-IA-2/blob/main/Banner.jpg?raw=true" style="width:700px;">

**Objetivo: Clasificar estrellas**




- Dataset: OGLE Project
- Modelo: Autoencoder


[(code)](https://github.com/HiperMaximus/Proyecto-IA-2) [(video)](xx) [(+info)](https://github.com/HiperMaximus/Proyecto-IA-2/blob/main/Presentaci%C3%B3n.pdf)

---

## Bees or Wasp” Dataset Classification using CNN’s with Transfer Learning via Fine-tuning <a name="proy10"></a>

**Autores: Camilo Andrés Calderón Carrillo, Jessica Paola Escobar Pérez, Daniel Felipe Rueda Mariño**

<img src="https://github.com/jessicaescobar21/beesvswasp/raw/main/banner.png" style="width:700px;">

**Objetivo: Implementar una red neuronal convolucional y aplicar técnicas como el aumento de datos, transferencia de aprendizaje y ajuste fino para clasificar el conjunto de datos "Bee vs Wasp"**




- Dataset: [bee-vs-wasp](https://www.kaggle.com/jerzydziewierz/bee-vs-wasp)
- Modelo: CNN


[(code)](https://github.com/jessicaescobar21/beesvswasp) [(video)](https://www.youtube.com/watch?v=DLFFQ5O1yZI&feature=youtu.be) [(+info)](https://github.com/jessicaescobar21/beesvswasp)

---

## Face detection mask <a name="proy11"></a>

**Autores: Sofia Torres, Sebastián García**

<img src="https://gitlab.com/sofiat9909/face-mask-detection/-/raw/master/images/banner-dimensiones.png" style="width:700px;">

**Objetivo: Detectar tapabocas en video"**




- Dataset: [face mask](https://www.kaggle.com/omkargurav/face-mask-dataset?select=data)
- Modelo: CNN+Haar features


[(code)](https://gitlab.com/sofiat9909/face-mask-detection) [(video)](https://www.youtube.com/watch?v=_bMRN4Xwu6M) [(+info)](https://gitlab.com/sofiat9909/face-mask-detection)

---
