---
# Proyectos 2022-2. Inteligencia Artificial II

## Prof: Fabio Martínez, Ph.D

---

# Lista de Proyectos
1. [Clasificando especies de aves según sus sonidos](#proy1)
2. [separación los materiales reciclables ](#proy2)
3. [Alert Tomato](#proy3)
4. [Fine-grained image classification](#proy4)
5. [Clasificación de fracturas de hueso](#proy5)
6. [Detección de fracturas en la espina cervical](#proy6)
7. [TuMoRIa](#proy7)
8. [Florificador](#proy8)
9. [Clasificacion de imagenes por tipos de melanomas](#proy9)
---


## Clasificando especies de aves según sus sonidos <a name="proy1"></a>

**Autores: Juan José Prenss Castro, Amin Esteban Barbosa Vargas, Brayan Sneider Daza Suárez**

<img src="https://github.com/MacheroKiller/ProyectoFinal_InteligenciaArtificial/raw/main/Banner/BannerAves.jpg" style="width:700px;">

**Objetivo:Identificar la especie de ave por medio del canto que emite.**

- Modelo:
- Data: Presenta una serie de audios de distintas especies de aves que comienzan desde la N a la Z. Los datos fueron sacados de la página Kaggle, que a su vez fueron recopilados de la página xeno-canto donde nos comparten el sonido que emiten diferentes tipos de animales. https://www.kaggle.com/datasets/rohanrao/xeno-canto-bird-recordings-extended-n-z https://xeno-canto.org/

[(code)](https://github.com/MacheroKiller/ProyectoFinal_InteligenciaArtificial) [(video)](https:) [(+info)](https:)
---

## separación de materiales reciclables <a name="proy2"></a>

**Autores: Paula Castro, Paula Hernaández**

<img src="https://user-images.githubusercontent.com/66750836/218624520-13a275ab-36e2-4a8d-bbd5-297ce326fa47.png" style="width:700px;">

**Objetivo:separar los materiales reciclables y ayudar a mejorar la eficiencia y la velocidad del proceso de reciclaje.**

- Modelo:CNN
[(code)](https://github.com/paulacandy/ProyectoIA2_2023/) [(video)](https://drive.google.com/drive/folders/1aY7Ic5nx18oaz-Nob9qrGz3qsdCuxaj4) [(+info)](https://github.com/paulacandy/ProyectoIA2_2023/)
---


## Alert Tomato <a name="proy3"></a>

**Autores: Erika Yamile Lache Blanco - 2140348, Dairon Alexis Vallejo Vallejo - 2171996**

<img src="https://camo.githubusercontent.com/a5f66790370d5321ed397328fb8cc8692369d08f85fca9349f7050189d6dae9b/68747470733a2f2f692e706f7374696d672e63632f3651364e667152702f42616e6e65722d4941322e706e67" style="width:700px;">

**Objetivo:Clasificar el tipo de enfermedad que puede sufrir la planta de tomate usando modelos de Deep learning a partir de características captadas por medio de las hojas de los tomates.**

- Modelo:
[(code)](https://github.com/erikalache97/Proyecto-IA2/blob/main/VGG16.ipynb) [(video)](https://www.youtube.com/watch?v=qbtBTRR-TT4&feature=youtu.be) [(+info)](https://github.com/erikalache97/Proyecto-IA2)
---


## Fine-grained image classification <a name="proy4"></a>

**Autores: Henry Dario Mantilla Claro**

<img src="https://github.com/HenryMantilla/ProyectoIA2/raw/main/Fine-grained%20classification.png" style="width:700px;">

**Objetivo:Profundizar en la clasificación de imágenes, más específicamente en aquellas clases difíciles de distinguir entre sí.**

- Modelo: MobileNetV2 pretrained CNN, Vision Transformer (ViT).
[(code)](https://github.com/HenryMantilla/ProyectoIA2) [(video)](https://youtu.be/pe98RpJVbkk) [(+info)](https://github.com/HenryMantilla/ProyectoIA2)
---

## Clasificación de fracturas de hueso <a name="proy1"></a>

**Autores: Yerson Stewell Ibarra Rueda, Jhordan Fabian Villamizar Peñaranda**

<img src="xxx" style="width:700px;">

**Objetivo:xxx**

- Modelo:
[(code)]() [(video)]() [(+info)](https://github.com/YrsonHTM/IA2)
---

## Detección de fracturas en espina cervical <a name="proy6"></a>

**Autores: Lewing Andres Mendez Ortiz 2162120, Diego Andres Lozada Niño 2170078**

<img src="https://github.com/anm2367l/Proyecto-IA2/blob/main/baner-ia2.png?raw=true" style="width:700px;">

**Objetivo:detectar fracturas en imagenes de CT**

- Modelo:
[(code)](https://github.com/anm2367l/Proyecto-IA2) [(video)]() [(+info)]()
---

## TuMoRIa <a name="proy1"></a>

**Autores: Daniel Eduardo Ortiz Celis - 2171469, Jefrey Steven Torres Garzón - 2190049, Camilo Eduardo González Guerrero - 2180065**

<img src="https://user-images.githubusercontent.com/20091905/219107350-33031d87-b3f6-4f8f-b5a7-3b5da8d8532f.png" style="width:700px;">

**Objetivo:Desarrollo de un modelo de aprendizaje profundo para la clasificación eficiente y fiable de diferentes tipos de tumores cerebrales.**

- Modelo:Deep Neural Networks (DNNs), Convolutional Neural Networks (CNNs), Data Augmentation, Transfer Learning, Dataset Fusion Autoencoders*,


[(code)](https://github.com/camilo64317/TuMoRIa) [(video)]( https://www.shorturl.at/euxCP) [(+info)](https://github.com/camilo64317/TuMoRIa)
---


## Florificador <a name="proy8"></a>

**Autores:  Ivan Andres Mendoza Oñate, 2182061 Yaire Catalina López Santana, 2182059 Alejandro Romero Serrano**

<img src="https://user-content.gitlab-static.net/371c5dae2accb8ae1e675b48f87b13ad019660df/68747470733a2f2f692e706f7374696d672e63632f584a3754393947622f62616e6e65722e706e67" style="width:700px;">

**Objetivo:clasificar algunos tipos de flores para ayudar a algunas ciencias que estudian plantas, como lo es la fenología, este proyecto es útil en casos como facilitar ela clasificacion de flores en el proceso de la polinización y así poder investigar al respecto para mejorar el proceso.**

- Modelo:
[(code)](https://gitlab.com/yairelopez/florificador) [(video)](https://www.youtube.com/watch?v=eTUoZRRSckU) [(+info)](https://gitlab.com/yairelopez/florificador)
---


## Clasificacion de imagenes por tipos de melanomas <a name="proy1"></a>

**Autores: Jose Pumarejo,Jhon Uribe**

<img src="https://github.com/josepuma02020/Proyecto-Intelgencia-Artificial---Clasificacion-de-imagenes-por-tipo-de-melanoma/raw/main/Banner.png" style="width:700px;">

**Objetivo:Visualizar y analizar imagenes que contienen los distintos tipos de melanomas y clasificarlas segun su tipo.**

- Modelo:DenseNet121

[(code)](https://colab.research.google.com/drive/1F9m5ElIPs9yf55lE5EQVGgPD_0aBhZgF#scrollTo=1fmXiKY7MwZF) [(video)](https://github.com/josepuma02020/Proyecto-Intelgencia-Artificial---Clasificacion-de-imagenes-por-tipo-de-melanoma) [(+info)](https://github.com/josepuma02020/Proyecto-Intelgencia-Artificial---Clasificacion-de-imagenes-por-tipo-de-melanoma)
---
