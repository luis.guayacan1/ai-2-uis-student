---
# Proyectos 2021-1. Inteligencia Artificial II. 

## Prof: Fabio Martínez, Ph.D

---

# Lista de Proyectos
1. [Clasificador de Covid-19 y Neumonía.](#proy1)
2. [PREDICCIÓN DE POSIBLE CÁNCER DE PRÓSTATA A PARTIR DE MRI POR MEDIO DE CNN 2D Y 3D.](#proy2)
3. [No soy un robot](#proy3)
4. [CLASIFICACIÓN DE PECES DE MAR POR MEDIO DE DNN Y CNN](#proy4)
5. [Pronóstico del comportamiento del precio accionario de distintas empresas que cotizan en bolsa](#proy5)
6. [Identificación de especies de serpientes mediante visión por computador](#proy6)
7. [Identificación de lesiones cutáneas para la detección de melanomas](#proy7)

## Clasificador de Covid-19 y Neumonía.<a name="proy1"></a>

**Autores:Daniel Alejandro Castillo Rodriguez, Julian Camilo Camacho Torres, Juan Sebastián Díaz Gutiérrez**

<img src="https://gitlab.com/i2919/proyecto-ia/-/raw/main/Banner_Proyecto.jpg?raw=true" style="width:700px;">

**Objetivo:Clasificar a partir de imagenes de rayos X toraxicas entre Covid-19 y Neumonía.**

- Dataset: Dos datasets con aproximadamente en conjunto 25000 imagenes de rayos x clasificadas en 4 categorias
- Modelo: Resnet, Alexnet, VGG, Squeezenet, Densenet, Inception, MobileNetV3, ChexNet

[(code)](https://gitlab.com/i2919/proyecto-ia/-/blob/main/Proyecto_IA2.ipynb) [(video)](https://www.youtube.com/watch?v=GRjDea5lry0) [(+info)](https://gitlab.com/i2919/proyecto-ia)

---

## PREDICCIÓN DE POSIBLE CÁNCER DE PRÓSTATA A PARTIR DE MRI POR MEDIO DE CNN 2D Y 3D..<a name="proy2"></a>
**Autores: Gabriel Felipe Vega Ochoa - 2170122, Andrés Felipe Cabeza Serrano - 2162106, Kevin Dlaikan Castillo - 2160090**

<img src="https://raw.githubusercontent.com/FelipeCabeza16/trabajo-final-IA2/main/Banner.jpg" style="width:700px;">

**Objetivo: Utilizar CNN para clasificar mediante el puntaje UCLA el nivel de significancia clínica de un posible cancer de próstata.**

- Dataset: Prostate and Ultrasound with pathology and coordinates of tracked biopsy
- Modelo: 2D, 3D

[(code)](https://github.com/FelipeCabeza16/trabajo-final-IA2) [(video)](https://drive.google.com/file/d/1ThtevYe9ZedQo6fW4Zxc8g49ghbvbiux/view) [(+info)](https://github.com/FelipeCabeza16/trabajo-final-IA2/blob/main/IAPres1.pdf)
---

## No soy un robot <a name="proy3"></a>

**Autores:Einer Steven Jaimes Mantilla, Luis Miguel Rodríguez**
<img src="https://camo.githubusercontent.com/bdfcea61437f19aa1eb827682c389bb05ca29abecff6c25956c906727fa8cb55/68747470733a2f2f692e6962622e636f2f565171324270332f62616e6e65722e706e67" style="width:700px;">

**Objetivo: Resolver captchas de google usando Unet y autoencoders.**
-Dataset: https://www.kaggle.com/fournierp/captcha-version-2-images
- Modelo: Deep learning, U-net, Autoencoder.

[(code)](https://github.com/luismiguel13/No-soy-un-robot) [(video)](https://www.youtube.com/watch?v=XqrT6woU-pw) [(+info)](https://github.com/luismiguel13/No-soy-un-robot)

##  CLASIFICACIÓN DE PECES DE MAR POR MEDIO DE DNN Y CNN  <a name="proy4"></a>
**Autores:William David Romero, Carlos Alberto Palencia Pombo, Hernando José Rojas**
<img src="https://raw.githubusercontent.com/wDavid98/IA2_Project_Fish_Dataset/main/banner.png" style="width:700px;">
**Objetivo: Evaluar el rendimiento de los diferentes parámetros propios de las redes convolucionales en la clasificación de peces.**
-Dataset: El dataset usado cuenta con 9 mil imágenes de 9 clases de peces, es decir, mil imágenes por cada clase, por otra parte también cuenta con 450 imagenes para testing, es decir 50 imagenes mas de cada una de las clases para validación de los modelos. Este dataset cuenta con un artículo en la IIIE y se puede encontrar en Kaggle: https://www.kaggle.com/dataset/3edc5a44084ff30d4045620abd6fb7843bee344f3bc3a6c3e98f309e4e624657
- Modelo:Redes Densas, Redes Convolucionales, Data augmentation ,transfer learning, autoencoder

[(code)](https://github.com/wDavid98/IA2_Project_Fish_Dataset) [(video)](https://www.youtube.com/watch?v=4w7UiC_lAHI) [(+info)](https://www.youtube.com/watch?v=4w7UiC_lAHI)

## Pronóstico del comportamiento del precio accionario de distintas empresas que cotizan en bolsa  <a name="proy5"></a>

**Autores: Edward Andres Sandoval Pineda (2180052), Sebastian Florez Rojas (2180050), Sebastian Pérez López (2180057)**

<img src="https://user-images.githubusercontent.com/74032759/158071917-80fe2038-0476-476c-84e0-c9d55396127d.png" style="width:700px;">

**Objetivo:Prediccion del maximo precio de salida de las acciones de 10 grandes empresas en intervalos de 5 minutos, tomando de entrada un ventaneo de 30 intervalos.**
-Dataset: Los datos se importan desde: Alpha Vantage API. Los cuales comprenden los precios de las acciones de 10 empresas influyentes cada 5 minutos por 2 años.
- Modelo:Modelos utilizados: CNN y RNN.
[(code)](https://github.com/edwardsandoval26/Neural_Networks-_Stock_Forecasting) [(video)](https://www.youtube.com/watch?v=9-56wym2NCg) [(+info)](https://github.com/edwardsandoval26/Neural_Networks-_Stock_Forecasting)


## Identificación de especies de serpientes mediante visión por computador  <a name="proy6"></a>

**Autores: Juan David Porras Gomez - 2172000, Jesús Daniel Lizcano Castro - 2171991**

<img src="https://raw.githubusercontent.com/juandavid04/ProyectoIA2/main/Banner/bannerIA2.PNG" style="width:700px;">

**Objetivo: Clasificar serpientes a nivel de especie con el fin de saber si son o no venenosas**

-Dataset: https://www.kaggle.com/goelyash/165-different-snakes-species
- Modelo: Convolutional Neuronal Network (CNN)
[(code)](https://github.com/juandavid04/ProyectoIA2) [(video)](https://www.youtube.com/watch?v=3J30jk0vQ0I) [(+info)](https://github.com/juandavid04/ProyectoIA2)

## Identificación de lesiones cutáneas para la detección de melanomas <a name="proy7"></a>

**Autores: Liliana Paola Castellanos Pinzón, Pedro Alfonso Jimenez Buenahora**

<img src="https://raw.githubusercontent.com/lipaocaspi/Skin_Cancer/main/Banner.png" style="width:700px;">

**Objetivo:Realizar la clasificación de diferentes tipos de lesiones cutáneas a través de una serie de imágenes, e identificar los melanomas, el tipo de cáncer de piel más mortal.**

-Dataset: Este dataset contiene 10015 imágenes dermatoscópicas de diferentes tipos de marcas cutáneas: Skin Cancer MNIST: HAM10000
-Modelo: MobileNet V2

[(code)](https://github.com/lipaocaspi/Skin_Cancer) [(video)](https://drive.google.com/drive/folders/1PWf1hL7rrW2RX91swgWUmBg1EQEVoKC1) [(+info)](https://github.com/lipaocaspi/Skin_Cancer)
