# Inteligencia Artificial II 2023-II

## ¡Bienvenidos!

<img src="/imgs/Banner_IA2_1.png" style="width:400px;">



## Colaboratory (Google)

Vamos a utilizar la plataforma de google para editar, compartir y correr notebooks: [**Colaboratory**](https://colab.research.google.com/notebooks/welcome.ipynb).

- Necesitas una cuenta de gmail y luego entras a drive.
- Colaboratory es un entorno de notebook de Jupyter gratuito que no requiere configuración y se ejecuta completamente en la nube.
    - Usaremos parte de la infraestructura de computo de google... **¡es gratis!** (máximo procesos de 8 horas).
- Con Colaboratory, puedes escribir, ejecutar código, guardar, compartir análisis y acceder a recursos informáticos potentes, todo gratis desde tu navegador.
- Puedes usar los recursos de computador local. 


## Calificación

- **15%** Talleres fundamentos y DNN (_Problemsets_).
- **15%** Talleres CNN (_Problemsets_).
- **15%** Talleres Detección de objetos (_Problemsets_).
- **15%** Talleres Autoencoders (_Problemsets_).
- **40%** Proyecto funcional IA.

## Talleres (Problemsets)

Los talleres pretenden ser una **herramienta practica** para afianzar los conocimientos desarrollados durante las clases. En general se presentan como un conjunto de ejercicios que serán desarrollados **individualmente** por los estudiantes. Cada taller esta escrito como un notebook para la validación automática. Se pueden hacer tantos intentos como se quieran y unicamente la última respuesta será tomada en cuenta. Cada uno de los talleres ser desarrollará de forma presencial, dentro de las fechas establecidas en el cronograma. 


## Proyecto funcional IA2

- **Aclaraciones**: El proyecto se debe realizar como un notebook de Python y debe ser **100% funcional**.

- **Fase 1 - Orientación (5%)**: Repositorio con link dataset y breve descripción. Además ya debe haber una validación de algunos modelos vistos en clase. 

- **Fase 2 - Prototipo y "PRE-SUS PROY" (15%)**: En este item se considera cómo está estructurado el proyecto. Se espera un nivel razonable de funcionalidad.

- **Fase 3 - Presentación "SUS PROY" (20%)**: Archivos a presentar:
    - Imagen relacionada (800 x 300) con la siguiente información: título del proyecto e información de los estudiantes.<br>
    - Video corto (Máximo 5 minutos) (alojarlo en youtube y entregar el enlace directo).<br>
    - Archivo que contenga las diapositivas. 
	

	**Sustentación**: Se realizarán preguntas cortas a los estudiantes unicamente relacionadas con el proyecto. 
 
	**Repositorio del proyecto** Todos los archivos relacionados con el proyecto deben alojarse en un repositorio alguno de los integrantes. Además, el archivo readme.md del repositorio debe tener la siguiente información:
	
    - Titulo del proyecto
    - Banner: Imagen de 800 x 300
	- Autores: Separados por comas
    - Objetivo: Una frase
	- Dataset: Link o instrucciones de descarga, y una breve descripción.
    - Modelos: Métodos usados para su desarrollo. Escribir solo palabras claves. 
    - Enlaces del código, video, repositorio y diapositivas.
        

**NOTA: UNICAMENTE SE TENDRAN EN CUENTA LOS PROYECTOS QUE SE HAYAN POSTULADO AL FINALIZAR EL PRIMER CORTE**-


## Calendario y plazos

                       SESSION 1                    SESSION 2


     W01 feb05-feb09    Intro IA2-DL                Fundamental-DNN
     W02 feb12-feb16    Fundamental-GraDe-BackPr    Fundamental-GraDe-BackPr
     W03 feb19-feb23    DNN-Reg_and_Backprop        DNN-Class_and_Backprop          
     W04 feb26-mar01    Práctica                    Práctica                  
     W05 mar04-mar08    Repaso                      __CHALLENGE 1__, **       *Proy: Fase 1
              			    -------------------------------------- 
     W06 sep28-sep29    CNN Fundamentals            CNN-tricks                
     W07 oct05-oct06    CNN_Transfer_learning       CNN_Transfer_learning
     W08 oct12-oct13    CNN_TL-aug-feat             __CHALLENGE 2__, **          
              			    --------------------------------------      
     W09 oct19-oct20    Autoencoders-CNN            Autoencoders-CNN, *       
     W10 oct26-oct27    Autoencoders-CNN            Autoencoders-CNN          *Proy: Fase 2     
     W11 nov02-nov03    Autoencoders-CNN            __CHALLENGE 3__, **       
     W12 nov09-nov10    __PRE-SUS PROYECTS__        __PRE-SUS PROYECTS__      
     W13 nov16-nov17    UNET                        GANS      
     W14 nov23-nov24    GANS			               RNN
              			    --------------------------------------           
     W15 nov30-dic01    RNN                        __CHALLENGE 4__
     W16 dic07-dic08    __SUS PROJ__               __SUS PROJ__               *Proy: Fase 3


     **Notas importantes:**

     Septiembre 29 		-> Registro primera nota
     Octubre 13       	-> Último día cancelación materias
     Diciembre 07      -> Finalización clase
     Diciembre 11-13   -> Evaluaciones finales
     Diciembre 13      -> Registro calificaciones finales
     Diciembre 14      -> Habilitaciones
     Diciembre 15      -> Registro de calificaciones definitivas



**ACUERDO n.° acuerdo 377, del 15 de noviembre de 2022**
[Calendario academico](https://uis.edu.co/wp-content/uploads/2022/12/Calendario-Academico-2023.pdf)

**DE SUMA IMPORTANCIA:**

- Cualquier entrega fuera de plazo será **penalizada con un 50%**
- Los problemsets están sujetos a cambios, los cuales serán **debidamente informado**
- El deadline de los problemsets será **el mismo día** de cada parcial.
- De ahora en adelante es necesario **TOMAR ASISTENCIA** de la clase. -> Con el 20% de fallas se pierde la asignatura en 0...
    - Las incapacidades se verifican por medio de bienestar universitario.
    - Para solicitar supletorio: por medio de la plataforma académica. Se hace por medio de "solicitudes académicas" y deben adjuntar el soporte. Es necesario tener en cuenta que se tienen máximo cinco días hábiles para solicitar el supletorio



